package mlaasserver

import (
	"testing"

	"golang.org/x/net/context"

	services "bitbucket.org/c3sr/mlaas-services"
	config "bitbucket.org/c3sr/p3sr-config"
)

func TestTrainModel(t *testing.T) {

	// Train the model with a specific data ID
	request := &services.TrainModelRequest{Userid: "test_user", Dataid: "test_dataid"}

	_, err := TrainModel(context.Background(), request)
	if err != nil {
		t.Error(err)
	}
}

func init() {
	config.Init()
}
