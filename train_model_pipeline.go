package mlaasserver

import (
	"golang.org/x/net/context"
	"google.golang.org/grpc"

	services "bitbucket.org/c3sr/mlaas-services"
	etcd "bitbucket.org/c3sr/p3sr-etcd-client"
	pg "bitbucket.org/c3sr/p3sr-grpc"
	log "github.com/Sirupsen/logrus"
)

func TrainModel(c context.Context, in *services.TrainModelRequest) (*services.TrainModelReply, error) {
	// Look up mlaas-db
	addr, err := etcd.FindService(services.DbServiceDescription)
	if err != nil {
		log.WithError(err).Error("couldn't find mlaas-db service through etcd. Is it running?")
		return &services.TrainModelReply{Modelid: ""}, err
	}
	log.Info("Using mlaas-db at addr ", addr)

	// Connect to the database service
	dbConn, err := pg.Dial(services.DbServiceDescription, addr, grpc.WithInsecure())
	if err != nil {
		log.WithError(err).Error("couldn't dial the mlaas-db service.")
		return &services.TrainModelReply{Modelid: ""}, err
	}
	defer dbConn.Close()
	dbClient := services.NewDbServiceClient(dbConn)

	// Load the requested dataid
	dbResp, err := dbClient.GetData(c, &services.GetDataRequest{Userid: in.Userid, Dataid: in.Dataid})
	if err != nil {
		log.WithError(err).Error("couldn't get data from mlaas-db service.")
		return &services.TrainModelReply{Modelid: ""}, err
	}

	// Look up the mlaas-nn
	addr, err = etcd.FindService(services.NNServiceDescription)
	if err != nil {
		log.WithError(err).Error("couldn't find mlaas-nn service. Is it running?")
		return &services.TrainModelReply{Modelid: ""}, err
	}
	log.Info("Using mlaas-db at addr ", addr)

	// Connect to the NN service
	nnConn, err := pg.Dial(services.NNServiceDescription, addr, grpc.WithInsecure())
	if err != nil {
		log.WithError(err).Error("couldn't dial the mlaas-nn service.")
		return &services.TrainModelReply{Modelid: ""}, err
	}
	defer nnConn.Close()
	nnClient := services.NewNNServiceClient(nnConn)

	// Instruct the NN service to train with the database
	nnReq := &services.TrainModelWithDataRequest{Data: dbResp.GetData()}
	modelRepl, err := nnClient.TrainModel(c, nnReq)
	if err != nil {
		log.WithError(err).Error("Model training with mlaas-nn produced an error.")
		return &services.TrainModelReply{Modelid: ""}, err
	}

	// Store the model with the mlaas-db
	storeModelRequest := &services.StoreModelRequest{Userid: in.Userid, Model: modelRepl}
	storeModelReply, err := dbClient.StoreModel(c, storeModelRequest)
	if err != nil {
		log.WithError(err).Error("Storing model with mlaas-db produced an error.")
		return &services.TrainModelReply{Modelid: ""}, err
	}

	return &services.TrainModelReply{Modelid: storeModelReply.Modelid}, nil
}
