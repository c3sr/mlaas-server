package mlaasserver

import (
	"golang.org/x/net/context"
	"google.golang.org/grpc"

	services "bitbucket.org/c3sr/mlaas-services"
	etcd "bitbucket.org/c3sr/p3sr-etcd-client"
	pg "bitbucket.org/c3sr/p3sr-grpc"
	log "github.com/Sirupsen/logrus"
)

func Inference(c context.Context, in *services.ClientInferenceRequest) (*services.InferenceReply, error) {
	// Look up mlaas-db
	addr, err := etcd.FindService(services.DbServiceDescription)
	if err != nil {
		log.WithError(err).Error("couldn't find mlaas-db service. Is it running?")
		return &services.InferenceReply{Class: ""}, err
	}
	log.Info("Using mlaas-db at addr ", addr)

	// Connect to the database service
	dbConn, err := pg.Dial(services.DbServiceDescription, addr, grpc.WithInsecure())
	if err != nil {
		log.WithError(err).Error("Unable to dial mlaas-db service at ", addr)
		return &services.InferenceReply{Class: ""}, err
	}
	defer dbConn.Close()
	dbClient := services.NewDbServiceClient(dbConn)

	// Load the requested model
	modelReq := &services.ModelRequest{Userid: in.GetUserid(), Modelid: in.GetModelid()}
	modelRepl, err := dbClient.GetModel(c, modelReq)
	if nil != err {
		log.WithError(err).Error("Got error response from mlaas-db service at ", addr)
		return &services.InferenceReply{Class: ""}, err
	}

	// Dial the tensorflow service
	tfAddr, err := etcd.FindService(services.NNServiceDescription)
	if err != nil {
		log.WithError(err).Error("couldn't find mlaas-tensorflow service. Is it running?")
		return &services.InferenceReply{Class: ""}, err
	}
	log.Info("Using mlaas-tf at addr ", addr)

	// Connect to the tensorflow service
	tfConn, err := pg.Dial(services.NNServiceDescription, tfAddr, grpc.WithInsecure())
	if err != nil {
		log.WithError(err).Error("Unable to dial mlaas-tensorflowservice at ", addr)
		return &services.InferenceReply{Class: ""}, err
	}
	defer tfConn.Close()
	tfClient := services.NewNNServiceClient(tfConn)

	// Generate a request for tensorflow service
	tfReq := &services.ServerInferenceRequest{Model: modelRepl.GetModel(), Data: in.GetData()}
	return tfClient.Inference(c, tfReq)
}
