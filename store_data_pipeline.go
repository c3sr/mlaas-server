package mlaasserver

import (
	"golang.org/x/net/context"
	"google.golang.org/grpc"

	services "bitbucket.org/c3sr/mlaas-services"
	etcd "bitbucket.org/c3sr/p3sr-etcd-client"
	pg "bitbucket.org/c3sr/p3sr-grpc"
	log "github.com/Sirupsen/logrus"
)

func StoreData(c context.Context, in *services.StoreDataRequest) (*services.StoreDataReply, error) {

	// Look up mlaas-db
	addr, err := etcd.FindService(services.DbServiceDescription)
	if err != nil {
		log.WithError(err).Error("couldn't find mlaas-db service. Is it running?")
		return &services.StoreDataReply{Id: ""}, err
	}
	log.Info("Using mlaas-db at addr ", addr)

	// Connect to the database service
	dbConn, err := pg.Dial(services.DbServiceDescription, addr, grpc.WithInsecure())
	if err != nil {
		log.WithError(err).Error("Unable to dial mlaas-db service at ", addr)
		return &services.StoreDataReply{Id: ""}, err
	}
	defer dbConn.Close()
	dbClient := services.NewDbServiceClient(dbConn)

	// Forward the request
	dbResp, err := dbClient.StoreData(c, in)
	if nil != err {
		log.WithError(err).Error("Got error response from mlaas-db service at ", addr)
	}

	return dbResp, err
}
