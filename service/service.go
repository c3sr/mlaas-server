package mlassserverservice

import (
	"errors"

	"golang.org/x/net/context"
	"google.golang.org/grpc"

	mlaasserver "bitbucket.org/c3sr/mlaas-server"
	services "bitbucket.org/c3sr/mlaas-services"
	pg "bitbucket.org/c3sr/p3sr-grpc"
	service "bitbucket.org/c3sr/p3sr-services/service"
	log "github.com/Sirupsen/logrus"
)

type Service struct {
	*service.Base
	server *grpc.Server
}

func New() *Service {
	desc := services.ServerServiceDescription
	return &Service{Base: service.NewBase(desc),
		server: pg.NewServer(desc),
	}
}

func (s *Service) Start() error {
	return service.Start(s)
}

func (s *Service) GetServer() *grpc.Server {
	return s.server
}

func (s *Service) StoreData(c context.Context, in *services.StoreDataRequest) (*services.StoreDataReply, error) {
	return mlaasserver.StoreData(c, in)
}

func (s *Service) StoreModel(c context.Context, in *services.StoreModelRequest) (*services.StoreModelReply, error) {
	return mlaasserver.StoreModel(c, in)
}

func (s *Service) TrainModel(c context.Context, in *services.TrainModelRequest) (*services.TrainModelReply, error) {
	log.Error("TrainModel unimplemented")
	return &services.TrainModelReply{Modelid: ""}, errors.New("TrainModel Unimplemented")
}

func (s *Service) Inference(c context.Context, in *services.ClientInferenceRequest) (*services.InferenceReply, error) {
	user := in.Userid
	model := in.Modelid
	data := in.Data
	log.Info("User: ", user, " Model: ", model, " ", len(data))

	return mlaasserver.Inference(c, in)
}

func (s *Service) ListModels(ctx context.Context, in *services.ListModelsRequest) (*services.StringsMessage, error) {
	return mlaasserver.ListModels(ctx, in)
}

func (s *Service) DeleteModel(ctx context.Context, in *services.ModelRequest) (*services.DeleteModelReply, error) {
	return mlaasserver.DeleteModel(ctx, in)
}
