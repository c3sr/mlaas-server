package mlaasserver

import (
	"testing"

	"golang.org/x/net/context"

	services "bitbucket.org/c3sr/mlaas-services"
	config "bitbucket.org/c3sr/p3sr-config"
	trace "bitbucket.org/c3sr/p3sr-trace"
)

func TestStoreData(t *testing.T) {

	request := &services.StoreDataRequest{Userid: "test_user", Data: []byte("test_payload")}
	_, err := StoreData(context.Background(), request)

	if err != nil {
		t.Error(err)
	}
}

func init() {
	config.Init()
	trace.Init("mlaas-store-data-pipeline-test")
}
