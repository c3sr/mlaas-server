package mlaasserver

import (
	"golang.org/x/net/context"
	"google.golang.org/grpc"

	services "bitbucket.org/c3sr/mlaas-services"
	etcd "bitbucket.org/c3sr/p3sr-etcd-client"
	pg "bitbucket.org/c3sr/p3sr-grpc"
	tracing "bitbucket.org/c3sr/p3sr-trace"
	log "github.com/Sirupsen/logrus"
)

// ListModels defines the pipeline the server uses to handle ListModels
func ListModels(ctx context.Context, in *services.ListModelsRequest) (*services.StringsMessage, error) {

	// Look up mlaas-db
	sp, ctx := tracing.StartSpanFromContext(ctx, "etcd_lookup_db")
	addr, err := etcd.FindService(services.DbServiceDescription)
	if err != nil {
		log.WithError(err).Error("couldn't find mlaas-db service. Is it running?")
		return &services.StringsMessage{Strings: []string{}}, err
	}
	sp.Finish()
	log.Info("Using mlaas-db at addr ", addr)

	// Connect to the database service

	sp, ctx = tracing.StartSpanFromContext(ctx, "etcd_dial_db")
	dbConn, err := pg.Dial(services.DbServiceDescription, addr, grpc.WithInsecure())

	if err != nil {
		log.WithError(err).Error("Unable to dial mlaas-db service at ", addr)
		return &services.StringsMessage{Strings: []string{}}, err
	}
	sp.Finish()
	defer dbConn.Close()
	dbClient := services.NewDbServiceClient(dbConn)

	// Forward the request to the database
	dbResp, err := dbClient.ListModels(ctx, in)
	if nil != err {
		log.WithError(err).Error("Got error response from mlaas-db service at ", addr)
	}

	return dbResp, err
}
