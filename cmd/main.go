package main

import (
	service "bitbucket.org/c3sr/mlaas-server/service"
	config "bitbucket.org/c3sr/p3sr-config"
	log "bitbucket.org/c3sr/p3sr-logger"
	tracing "bitbucket.org/c3sr/p3sr-trace"
)

func main() {
	config.Init() // initialize etcd and so forth
	log.Info("Initialized p3sr")

	s := service.New()

	// Create a new tracer for the server to use
	tracer, closer := tracing.New("mlaas-server")
	defer closer.Close()
	tracing.InitGlobalTracer(tracer)

	err := s.Start()
	if err != nil {
		log.WithError(err).Fatal("Unable to start mlaas-server service")
	}

}
